from fastapi import FastAPI, Request, Response
import requests
from domain import *
from fastapi.middleware.cors import CORSMiddleware

app = FastAPI()

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

@app.post("/api/v1/products")
async def create_product(request: Request):
    data = await request.json()
    response_1 = requests.post(f"{PROFILE_DOMAIN}/auth/verify-token", headers=request.headers)
    if not response_1.ok:
        return Response(response_1.content, status_code=response_1.status_code)
    response = requests.post(PRODUCT_DOMAIN, json=data)
    return Response(response.content, status_code=response.status_code)
